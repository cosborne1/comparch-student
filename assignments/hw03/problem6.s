.set noreorder
.data
#int A[8];
.text
.globl main
.ent main
main:
#int main()
#{
	addi 	$sp, $sp, -40 	#add 40 spaces of memory to stack pointer
	sw 		$ra, 36($sp)	# puts ra on stack
#	int i;
#	A[0] = 0;
	addi 	$t0, $0, 0 	# set t0 = 0
	sw 		$t0, 0($sp) # set A[0] on stack
#	A[1] = 1;
	addi 	$t0, $0, 1  # set t0 = 1
	sw 		$t0, 4($sp) 	# set A[1] on stack
#	for (i = 2; i < 8; i++) {
	addi 	$t0, $0, 2 	# set t0 = i = 2
	sw 		$t0, 32($sp) 	# puts i on stack
	

loop:
	slti 	$t2, $t0, 8 # set t2 = 1 if i < 8
	beq 	$t2, $0, END 	# go to the end if i > 8
	nop
#		A[i] = A[i-1] + A[i-2];
		lw 		$s0, 4($sp) 	# puts A[i-1] into s0
		lw 		$s1, 0($sp) 	# puts A[i-2] into s1
		add 	$s2, $s0, $s1 	# A[i] = s2
		sw 		$s2, 8($sp) 	# put A[i] on the stack
#		PRINT_HEX_DEC(A[i]);
		jal 	Print
		nop

		addi 	$t0, $t0, 1 	# increment i by 1

		j loop 	# jump to the beginning of loop
		nop
#
#	}
Print:
	lw 		$a0, 8($sp) 	# loads a[i] for printing
	ori 	$v0, $0, 20 	# print a[i]
	syscall
	lw 		$t3, 4($sp) 	# puts a[i-1] into t3
	sw 		$t3, 0($sp) 	# puts a[i-1] into a[i-2]
	sw 		$a0, 4($sp) 	# puts a[i] into a[i-1]
	jr $ra 					# return to return address
	nop

END:
#	EXIT;
#
#}
lw 	$ra, 36($sp) 	# get ra back
addi 	$sp, $sp, 40 	# reset stack pointer
jr 	$ra 			# jump back
nop
.end main
