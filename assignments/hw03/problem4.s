.set noreorder
.data

# typedef struct record {
# int field0;
# int field1;
# } record;

record.sizeof = 8 	# the size of a record
record.field0 = 0 	# the size of a record's field 1
record.field1 = 4 	# the size of a record's field 2



.text
.globl main
.ent main
main:
# int main()

	# record *r = (record *) MALLOC(sizeof(record));
	mallocedRecord: .space record.sizeof # allocates space for the malloced record
	r:      		.space 4 # allocates space for the pointer r
	la $t0, mallocedRecord 	# t0 is set to the address of mallocedRecord
	la $t1, r 		# t1 is set to the address of r
	sw $t0, ($t1) 	# the address in r now points to t0 (mallocedRecord)

	# r->field0 = 100;
	addi $t2, $0, 100 	# t2 = 100
	sw $t2, record.field0($t1) 	# 100 is saved into field 0 of the address the pointer is pointing to

	# r->field1 = -1;
	addi $t3, $0, -1 	# t3 = -1
	sw $t3, record.field1($t1) 	# -1 is saved into field 1 of the address the pointer is pointing to

	ori $v0, $0, 10         # exit
	syscall
# EXIT;
	.end main
