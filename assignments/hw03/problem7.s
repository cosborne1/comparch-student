.set noreorder
.data
.text
.globl main
.ent main
main:
#int main()
#{
	addi $sp, $sp, -8 	# allocate space onto stack pointer
#	int a = 2;
	addi $s0, $0, 2 	# a = s0 = 2
	sw 	 $ra, 4($sp) 	# ra saved to stack
	sw 	 $s0, 0($sp) 	# a saved to stack
#	int f = polynomial(a, 3, 4, 5, 6);
	addi $a0, $s0, 0 	# a0 = a
	addi $a1, $0, 3 	# a1 = 3
	addi $a2, $0, 4 	# a2 = 4
	addi $a3, $0, 5 	# a3 = 5
	addi $t5, $0, 6 	# t5 = 6
	jal polynomial 		# go to polynomial
	nop
	add  $s1, $v0, $0 	# s1 = f
	lw 	 $ra, 4($sp) 	# load ra
	lw 	 $s0, 0($sp) 	# load s0
	addi $sp, $sp, 8 	# reset stack pointer
#	print(a);
	addi $sp, $sp, -12 	# allocate space onto stack pointer
	sw 	 $ra, 8($sp) 	# ra saved to stack
	sw 	 $s0, 4($sp) 	# a saved to stack
	sw 	 $s1, 0($sp) 	# f saved to stack
	addi $a0, $s0, 0 	# a0 = a
	jal print 
	nop
	lw 	 $ra, 8($sp) 	# load ra
	lw 	 $s0, 4($sp) 	# load s0
	lw 	 $s0, 0($sp) 	# load s1
	addi $sp, $sp, 12 	# reset stack pointer
#	print(f);
	addi $sp, $sp, -12 	# allocate space onto stack pointer
	sw 	 $ra, 8($sp) 	# ra saved to stack
	sw 	 $s0, 4($sp) 	# a saved to stack
	sw 	 $s1, 0($sp) 	# f saved to stack
	addi $a0, $s1, 0 	# a0 = f
	jal print 
	nop
	lw 	 $ra, 8($sp) 	# load ra
	lw 	 $s0, 4($sp) 	# load s0
	lw 	 $s0, 0($sp) 	# load s1
	addi $sp, $sp, 12 	# reset stack pointer
	jr $ra 				# jump to exit
	nop
#
#}
print:
#void print(int a)
#{
	ori 	$v0, $0, 20 	# print a
	syscall
	jr 		$ra 		 	# return
	nop
#	// should be implemented with syscall 20
#
#}
sum3:
#int sum3(int a, int b, int c)
#{
	add 	$t0, $a0, $a1 	# t0 = a + b
	add 	$t1, $t0, $a2 	# t1 = t0 + c
#	return a+b+c;
	add 	$v0, $0, $t1 	# v0 is returned
	jr 		$ra 			# return
	nop
#
#}
polynomial:
#int polynomial(int a, int b, int c, int d, int e)
#{
#	int x;
#	int y;
#	int z;
#	x = a << b;
	sllv 	$s0, $a0, $a1 	# s0 (x) = a << b 
#	y = c << d;
	sllv 	$s1, $a2, $a3 	# s1 (y) = c << d
#	z = sum3(x, y, e);
	addi 	$sp, $sp, -32 	# allocate space for stack pointer
	sw 		$a0, 0($sp) 	# a0 onto stack
	sw 		$a1, 4($sp) 	# a1 onto stack
	sw 		$a2, 8($sp) 	# a2 onto stack
	sw 		$a3, 12($sp) 	# a3 onto stack
	sw 		$t5, 16($sp) 	# t5 onto stack
	sw 		$s0, 20($sp) 	# s0 (x) onto stack
	sw 		$s1, 24($sp) 	# s1 (y) onto stack
	sw 		$ra, 28($sp) 	# ra onto stack
	addi $a0, $s0, 0 	# a0 = x
	add $a1, $0, $s1 	# a1 = y
	add $a2, $0, $t5 	# a2 = e
	jal sum3 			# go to sum3
	nop
	add 	$s2, $v0, $0 	# s2 = z

	lw 		$a0, 0($sp) 	# reload a0
	lw 		$a1, 4($sp) 	# reload a1
	lw 		$a2, 8($sp) 	# reload a2
	lw 		$a3, 12($sp) 	# reload a3
	lw 		$t5, 16($sp) 	# reload t5
	lw 		$s0, 20($sp) 	# reload s0
	lw 		$s1, 24($sp) 	# reload s1
	lw 		$ra, 28($sp) 	# reload ra
	addi 	$sp, $sp, 32 	# reset sp
	
#	print(x);
	
	addi 	$sp, $sp, -36 	# allocate space for stack pointer
	sw 		$a0, 0($sp) 	# a0 onto stack
	sw 		$a1, 4($sp) 	# a1 onto stack
	sw 		$a2, 8($sp) 	# a2 onto stack
	sw 		$a3, 12($sp) 	# a3 onto stack
	sw 		$t5, 16($sp) 	# t5 onto stack
	sw 		$s0, 20($sp) 	# s0 (x) onto stack
	sw 		$s1, 24($sp) 	# s1 (y) onto stack
	sw 		$s2, 28($sp) 	# s2 (z) onto stack
	sw 		$ra, 32($sp) 	# ra onto stack
	add 	$a0, $s0, $0 	# a0 = x
	jal print 				# go to print
	nop
	lw 		$a0, 0($sp) 	# reload a0
	lw 		$a1, 4($sp) 	# reload a1
	lw 		$a2, 8($sp) 	# reload a2
	lw 		$a3, 12($sp) 	# reload a3
	lw 		$t5, 16($sp) 	# reload t5
	lw 		$s0, 20($sp) 	# reload s0
	lw 		$s1, 24($sp) 	# reload s1
	lw 		$s2, 28($sp) 	# reload s2
	lw 		$ra, 32($sp) 	# reload ra
	addi 	$sp, $sp, 36 	# reset sp
#	print(y);
	addi 	$sp, $sp, -36 	# allocate space for stack pointer
	sw 		$a0, 0($sp) 	# a0 onto stack
	sw 		$a1, 4($sp) 	# a1 onto stack
	sw 		$a2, 8($sp) 	# a2 onto stack
	sw 		$a3, 12($sp) 	# a3 onto stack
	sw 		$t5, 16($sp) 	# t5 onto stack
	sw 		$s0, 20($sp) 	# s0 (x) onto stack
	sw 		$s1, 24($sp) 	# s1 (y) onto stack
	sw 		$s2, 28($sp) 	# s2 (z) onto stack
	sw 		$ra, 32($sp) 	# ra onto stack
	add 	$a0, $s1, $0 	# a0 = y
	jal print 				# go to print
	nop
	lw 		$a0, 0($sp) 	# reload a0
	lw 		$a1, 4($sp) 	# reload a1
	lw 		$a2, 8($sp) 	# reload a2
	lw 		$a3, 12($sp) 	# reload a3
	lw 		$t5, 16($sp) 	# reload t5
	lw 		$s0, 20($sp) 	# reload s0
	lw 		$s1, 24($sp) 	# reload s1
	lw 		$s2, 28($sp) 	# reload s2
	lw 		$ra, 32($sp) 	# reload ra
	addi 	$sp, $sp, 36 	# reset sp
#	print(z);
	addi 	$sp, $sp, -36 	# allocate space for stack pointer
	sw 		$a0, 0($sp) 	# a0 onto stack
	sw 		$a1, 4($sp) 	# a1 onto stack
	sw 		$a2, 8($sp) 	# a2 onto stack
	sw 		$a3, 12($sp) 	# a3 onto stack
	sw 		$t5, 16($sp) 	# t5 onto stack
	sw 		$s0, 20($sp) 	# s0 (x) onto stack
	sw 		$s1, 24($sp) 	# s1 (y) onto stack
	sw 		$s2, 28($sp) 	# s2 (z) onto stack
	sw 		$ra, 32($sp) 	# ra onto stack
	add 	$a0, $s2, $0 	# a0 = z
	jal print 				# go to print
	nop
	lw 		$a0, 0($sp) 	# reload a0
	lw 		$a1, 4($sp) 	# reload a1
	lw 		$a2, 8($sp) 	# reload a2
	lw 		$a3, 12($sp) 	# reload a3
	lw 		$t5, 16($sp) 	# reload t5
	lw 		$s0, 20($sp) 	# reload s0
	lw 		$s1, 24($sp) 	# reload s1
	lw 		$s2, 28($sp) 	# reload s2
	lw 		$ra, 32($sp) 	# reload ra
	addi 	$sp, $sp, 36 	# reset sp
#	return z;
	add 	$v0, $s2, $0 	# returning z
	jr 		$ra 			# go back to return address
	nop
#
#}
.end main
