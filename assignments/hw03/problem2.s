
.set noreorder
.data
#char A[] = { 1, 25, 7, 9, -1  };
A: 	.byte 	1, 25, 7, 9, -1 	#create array A as global variable
.text
.globl main
.ent main
main:
#int main()
#{
#	int i;
#	int current;
#	int max;
#	i = 0;
	addi 	$t0, $0, 0 	# t0 = i = 0
#	current = A[0];
	la 		$t1, A 		# Puts address of A into t1
	lb 		$t2, 0($t1) # t2 = current = A[0]
#	max = 0;
	addi 	$t3, $0, 0 	# t3 = max = 0
while:
#	while (current > 0) {
	slti 	$t4, $t2, 1 # t4 = 1 if current < 1
	bne 	$t4, $0, End 	# jump to the end if current < 1
	nop
#		if (current > max)
		slt 	$t4, $t3, $t2 	# t4 = 1 if max < current 
		beq 	$t4, $0, afterloop 	# go to afterloop if current <= max
		nop
#			max = current;
			addi 	$t8, $0, 300
			addi 	$t3, $t2, 0 	# max = current
afterloop:
#		i = i + 1;
		addi 	$t0, $t0, 1 	# i = i + 1
#		current = A[i];
		lb 	 	$t2, A($t0) 	# t2 = current = A[i]
		j 	while
		nop
#
#	}
End:
#	PRINT_HEX_DEC(max);
	addi 	$a0, $t3, 0 	# set a0 to max
	ori 	$v0, $0, 20 	# print max in hex and decimal
	syscall
	ori $v0, $0, 10     # exit
	syscall
#	EXIT;
#
#}
.end main
