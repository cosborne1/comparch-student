.set noreorder
.data
#typedef struct elt {
#	int value;
#	struct elt *next;
#} elt;

elt.sizeof = 8
elt.value = 0
elt.next = 4
.text
.globl main
.ent main
main:
#int main()
#{
#	elt *head;
#	elt *newelt;
#	newelt = (elt *) MALLOC(sizeof (elt));
	addi 	$a0, $0, elt.sizeof 	# size to malloc goes in a0
	ori 	$v0, $0, 9 				# tells syscall to malloc
	syscall
	or 		$s0, $v0, $0 		# Makes s0 a pointer to newly malloced space
#	newelt->value = 1;
	addi 	$t0, $0, 1 		# creates temporary value of 1
	sw 		$t0, elt.value($s0) 	# stores 1 at newelt's value
#	newelt->next = 0;
	sw 		$0, elt.next($s0) 		# stores 0 at newelt's next
#	head = newelt;
	or 		$s1, $s0, $0 		# set s1 (head) equal to s0 (newelt)
#	newelt = (elt *) MALLOC(sizeof (elt));
	addi 	$a0, $0, elt.sizeof 	# size to malloc goes in a0
	ori 	$v0, $0, 9 				# tells syscall to malloc
	syscall
	or 		$s0, $v0, $0 		# Makes s0 a pointer to newly malloced space
#	newelt->value = 2;
	addi 	$t0, $0, 2 		# creates temporary value of 2
	sw 		$t0, elt.value($s0) 	# stores 2 at newelt's value
#	newelt->next = head;
	sw 		$s1, elt.next($s0) 		# stores head at newelt's next
#	head = newelt;
	or 		$s1, $s0, $0 		# set s1 (head) equal to s0 (newelt)
#	PRINT_HEX_DEC(head->value);
	lw 		$a0, elt.value($s1) # load a0 with head->value
	ori 	$v0, $0, 20 		# print head->value
	syscall
#	PRINT_HEX_DEC(head->next->value);
	lw 		$t0, elt.next($s1) 	# load t0 with head->next
	lw 		$a0, elt.value($t0) # load a0 with head->next->value
	ori 	$v0, $0, 20 		# print head->value
	syscall
#	EXIT;
#
#}
ori $v0, $0, 10     # exit
syscall
.end main
