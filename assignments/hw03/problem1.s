#problem1.s
.set noreorder
.data
.text
.globl main
.ent main
main:
#int main()
#{
#	int score = 84;
	addi 	$t0, $0, 84 	# t0 = score = 84
#	int grade;
#	if (score >= 90)
	slti 	$t2, $t0, 90 	# t2 = 1 if score < 90
	bne 	$t2, $0, Check2 # Go to check 2 if t2 = 1 
	nop
#		grade = 4;
	addi 	$t1, $0, 4 		# t1 = 4
	j 		End 			# loop is done, jump to printing
	nop
Check2:
#	else if (score >= 80)
	slti 	$t2, $t0, 80 	# t2 = 1 if score < 80
	bne 	$t2, $0, Check3 # Go to check 3 if t2 = 1
	nop
#		grade = 3;
	addi 	$t1, $0, 3 		# t1 = 3
	j 		End 			# loop is done, jump to printing
	nop
Check3:
#	else if (score >= 70)
	slti 	$t2, $t0, 70 	# t2 = 1 if score < 70
	bne 	$t2, $0, Check4 # Go to Check4 if t2 = 1
	nop
#		grade = 2;
	addi 	$t1, $0, 2 		# t1 = 2
	j 		End 			# loop is done, jump to end
	nop
Check4:
#	else
	addi 	$t1, $0, 0 		# Else, t1 = 0
#		grade = 0;
End:
#	PRINT_HEX_DEC(grade);
	add 	$a0, $0, $t1 	# Setting up $a0 with the vlaue of grade for the syscall
	ori 	$v0, $0, 20     # Setting up $v0 for the printing hex and decimal syscall
	syscall 				# Actual syscall

ori $v0, $0, 10     # exit
syscall
#	EXIT;
#
#}

.end main

