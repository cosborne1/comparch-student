.set noreorder
.data
#int A[8];
A: 	.space 32
.text
.globl main
.ent main
main:
#int main()
#{
#	int i;
#	A[0] = 0;
	la 		$t1, A 		# set t1 = address of A
	addi 	$t0, $0, 0 	# set t0 = 0
	sw 		$t0, 0($t1) # set A[0] = 0
#	A[1] = 1;
	addi 	$t0, $0, 1  # set t0 = 0
	sw 		$t0, 4($t1) # set A[1] = 1
#	for (i = 2; i < 8; i++) {
	addi 	$t0, $0, 2 	# set t0 = i = 2

	addi 	$t7, $0, 8 		# t7 = 8, this is the shifter
loop:
	slti 	$t2, $t0, 8 # set t2 = 1 if i < 8
	beq 	$t2, $0, END 	# go to the end if i > 8
	nop
#		A[i] = A[i-1] + A[i-2];
		addi	$t3, $t7, -4 	# t3 = (i-1)*4
		addi 	$t4, $t7, -8	# t4 = (i-2)*4
		lw 		$t5, A($t3)	    # t5 = A(t3)
		lw 		$t6, A($t4)		# t6 = A(t4)
		add 	$a0, $t5, $t6	# a0 = t5 + t6
		sw 		$a0, A($t7) 	# A[t7] = a0
#		PRINT_HEX_DEC(A[i]);
		ori 	$v0, $0, 20 	# print a[i]
		syscall

		addi 	$t0, $t0, 1 	# increment i by 1
		addi 	$t7, $t7, 4 	# increment t7 by 4

		j loop 	# jump to the beginning of loop
		nop
#
#	}
END:
#	EXIT;
#
#}
ori $v0, $0, 10     # exit
syscall
.end main
